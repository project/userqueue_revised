<?php

/**
 * @file
 * Menu callback, arrange userqueue_revised form
 */
function userqueue_revised_view_queue($queue) {
  drupal_set_title($queue->title);
  $users = userqueue_revised_get_users($queue);
  return drupal_get_form('userqueue_revised_arrange_subqueue_form', $queue, $users);
}

/**
 * Menu callback, remove user from a userqueue
 */
function userqueue_revised_admin_remove_user($queue, $account) {
  db_delete('userqueue_revised_user')
      ->condition('uid', $account->uid)
      ->condition('uqid', $queue->uqid)
      ->execute();
  drupal_goto($_GET['destination']);
}

/**
 * Form definition for userqueue drag'n'drop form.
 */
function userqueue_revised_arrange_subqueue_form($form, $form_state, $queue, $users) {
  $form = array('#tree' => TRUE);
  // Prepare the main part of the form which will be themed as a table.
  $count = count($users);
  $form['users'] = array();
  $form['users']['#theme'] = 'userqueue_revised_arrange_subqueue_form_table';

  // Theme function needs these.
  $form['users']['#queue'] = (array) $queue;
  foreach ($users as $account) {
    $form['users'][$account->uid]['#user'] = (array) $account;
    $form['users'][$account->uid]['title'] = array('#markup' => theme('username', array('account' => $account)));
    $form['users'][$account->uid]['date'] = array('#markup' => format_date($account->created, 'short'));
    $form['users'][$account->uid]['edit'] = array('#markup' => l(t('edit'), 'user/' . $account->uid . '/edit', array('attributes' => array('title' => t('Edit this user')))));
    $form['users'][$account->uid]['position'] = array(
      '#type' => 'position',
      '#delta' => $count,
      '#default_value' => $account->position,
      '#attributes' => array(
        'class' => array('user-position'),
      ),
    );
    $attr = array(
      '#attributes' => array(
        'title' => t('Remove from queue'),
        'style' => 'display: none;',
        'class' => array('userqueue-remove'),
        'id' => 'userqueue-remove-' . $account->uid,
      ),
      'query' => drupal_get_destination(),
    );
    $form['users'][$account->uid]['remove'] = array('#markup' => l(t('remove'), 'userqueue_revised/' . $queue->uqid . '/remove-user/' . $account->uid, $attr));
  }

  // Textfield for adding users to the queue.
  $form['add'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['add']['user'] = array(
    '#type' => 'textfield',
    '#autocomplete_path' => 'user/autocomplete',
    '#maxlength' => 1024,
  );
  $form['add']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add user'),
    '#submit' => array('userqueue_revised_arrange_subqueue_form_add_submit'),
  );
  // Submit, reverse, shuffle, and clear actions.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  // Disable all buttons if the queue is empty.
  if ($count == 0) {
    $form['actions']['submit']['#disabled'] = TRUE;
  }
  print_r($form); die();
  return $form;
}

/**
 * Submit handler, add a new user in the queue
 */
function userqueue_revised_arrange_subqueue_form_add_submit($form, $form_state) {
  $val = $form_state['values']['add'];
  $uid = db_select('users', 'u')
      ->fields('u', array('uid'))
      ->condition('name', $val['user'])
      ->execute()
      ->fetchField();
  $uqid = $form['users']['#queue']['uqid'];
  $query = db_select('userqueue_revised_user', 'uu');
  $query->addExpression('MAX(position)', 'position');
  $position = $query->execute()->fetchField();
  if ($uid) {
    $item = new stdClass();
    $item->uid = $uid;
    $item->position = ($position + 1);
    $item->uqid = $uqid;
    drupal_write_record('userqueue_revised_user', $item);
    drupal_set_message(t('User %username has been added', array('%username' => $val['user'])));
  }
}

/**
 * Submit handler,
 */
function userqueue_revised_arrange_subqueue_form_submit($form, $form_state) {
  $users = array();
  foreach ($form_state['values']['users'] as $uid => $element) {
    $users[$form_state['values']['users'][$uid]['position']] = array(
      'uid' => $uid,
      'data' => $form_state['values']['users'][$uid]
    );
  }
  $uqid = $form['users']['#queue']['uqid'];
  userqueue_revised_save_queue_order($users, $uqid);
}

/**
 * Validates new queue order information and if it passes validation,
 * deletes the old queue data from the database and saves the new data.
 */
function userqueue_revised_save_queue_order($users, $uqid) {
  $positions = array();
  $now = REQUEST_TIME;
  $queue = userqueue_revised_load($uqid);

  // cleanup the node array
  $clean = array();
  $count = 1;
  ksort($users);
  foreach ($users as $pos => $account) {
    if (is_numeric($pos)) {
      $clean[$count] = $account;
      $count ++;
    }
  }
  $users = $clean;
  // clear the queue and save the new positions
  db_delete('userqueue_revised_user')
      ->condition('uqid', $uqid)
      ->execute();

  foreach ($users as $pos => $user) {
    $args = array();
    $query = db_insert('userqueue_revised_user')
        ->fields(array(
          'uqid' => $uqid,
          'uid' => $user['uid'],
          'position' => $pos,
        ))
        ->execute();
  }
  return drupal_set_message(t('The queue has been updated.'));
}
